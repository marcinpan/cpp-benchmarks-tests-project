#include "benchmark/benchmark.h"
using namespace benchmark;
#include "Small.h"
#include "Medium.h"
#include "Large.h"
#include <deque>



/*SMALL*/

void smallDequeAt(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        DoNotOptimize(deque.at(randNum));

    }

    state.SetComplexityN(N);
}

BENCHMARK(smallDequeAt)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();


void smallDequeOperator(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        // Only for Release mode - without that optimizer will remove our code
        DoNotOptimize(deque[randNum]);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeOperator)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();


void smallDequeFront(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.front());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeFront)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeBack(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.back());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeBack)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();


void smallDequeEmpty(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    small.randomize();
    deque.push_back(small);
    while (state.KeepRunning()) {
        DoNotOptimize(deque.empty());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeEmpty)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeSize(State& state) {
    auto N = state.range(0);

    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeSize)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeMaxSize(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    while (state.KeepRunning()) {
        DoNotOptimize(deque.max_size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeMaxSize)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeShrinkToFit(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    deque.clear();
    while (state.KeepRunning()) {
        deque.shrink_to_fit();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeShrinkToFit)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeClear(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }

    while (state.KeepRunning()) {
        deque.clear();
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeClear)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeInsert(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_back(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.insert(deque.begin() + rand()%N, small);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeInsert)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeErase(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        auto random = rand()%N;
        deque.insert(deque.begin() + random, small);
//        state.ResumeTiming();
        deque.erase(deque.begin() + random);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeErase)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequePushBack(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.push_back(small);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequePushBack)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequePopBack(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
        small.randomize();
        deque.push_back(small);
//        state.PauseTiming();
        deque.pop_front();
//        state.ResumeTiming();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequePopBack)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequePushFront(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        deque.pop_back();
//        state.ResumeTiming();
        deque.push_front(small);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequePushFront)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();


void smallDequePopFront(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        deque.push_front(small);
//        state.ResumeTiming();
        deque.pop_front();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequePopFront)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeResize(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque.push_front(small);
    }
    while (state.KeepRunning()) {
        deque.resize(rand()%N);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeResize)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();

void smallDequeSwap(State& state) {
    auto N = state.range(0);
    std::deque<Small> deque1, deque2;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        deque1.push_back(small);
        small.randomize();
        deque2.push_back(small);
    }
    while (state.KeepRunning()) {
        deque1.swap(deque2);
        DoNotOptimize(deque1);
        DoNotOptimize(deque2);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallDequeSwap)->RangeMultiplier(2)->Range(1, 1<<21)->Complexity();


/*MEDIUM*/

void mediumDequeAt(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        DoNotOptimize(deque.at(randNum));

    }

    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeAt)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();


void mediumDequeOperator(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        // Only for Release mode - without that optimizer will remove our code
        DoNotOptimize(deque[randNum]);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeOperator)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();


void mediumDequeFront(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.front());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeFront)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeBack(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.back());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeBack)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();


void mediumDequeEmpty(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    medium.randomize();
    deque.push_back(medium);
    while (state.KeepRunning()) {
        DoNotOptimize(deque.empty());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeEmpty)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeSize(State& state) {
    auto N = state.range(0);

    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeSize)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeMaxSize(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    while (state.KeepRunning()) {
        DoNotOptimize(deque.max_size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeMaxSize)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeShrinkToFit(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    deque.clear();
    while (state.KeepRunning()) {
        deque.shrink_to_fit();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeShrinkToFit)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeClear(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }

    while (state.KeepRunning()) {
        deque.clear();
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeClear)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeInsert(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_back(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.insert(deque.begin() + rand()%N, medium);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeInsert)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeErase(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        auto random = rand()%N;
        deque.insert(deque.begin() + random, medium);
//        state.ResumeTiming();
        deque.erase(deque.begin() + random);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeErase)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequePushBack(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.push_back(medium);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequePushBack)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequePopBack(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
        medium.randomize();
        deque.push_back(medium);
//        state.PauseTiming();
        deque.pop_front();
//        state.ResumeTiming();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequePopBack)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequePushFront(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        deque.pop_back();
//        state.ResumeTiming();
        deque.push_front(medium);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequePushFront)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();


void mediumDequePopFront(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        deque.push_front(medium);
//        state.ResumeTiming();
        deque.pop_front();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequePopFront)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeResize(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque.push_front(medium);
    }
    while (state.KeepRunning()) {
        deque.resize(rand()%N);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeResize)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();

void mediumDequeSwap(State& state) {
    auto N = state.range(0);
    std::deque<Medium> deque1, deque2;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        deque1.push_back(medium);
        medium.randomize();
        deque2.push_back(medium);
    }
    while (state.KeepRunning()) {
        deque1.swap(deque2);
        DoNotOptimize(deque1);
        DoNotOptimize(deque2);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumDequeSwap)->RangeMultiplier(2)->Range(1, 1<<15)->Complexity();
/*LARGE*/
//
void largeDequeAt(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        DoNotOptimize(deque.at(randNum));

    }

    state.SetComplexityN(N);
}

BENCHMARK(largeDequeAt)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();


void largeDequeOperator(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        // Only for Release mode - without that optimizer will remove our code
        DoNotOptimize(deque[randNum]);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeOperator)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();


void largeDequeFront(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.front());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeFront)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeBack(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.back());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeBack)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();


void largeDequeEmpty(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    large.randomize();
    deque.push_back(large);
    while (state.KeepRunning()) {
        DoNotOptimize(deque.empty());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeEmpty)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeSize(State& state) {
    auto N = state.range(0);

    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(deque.size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeSize)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeMaxSize(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    while (state.KeepRunning()) {
        DoNotOptimize(deque.max_size());
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeMaxSize)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeShrinkToFit(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    deque.clear();
    while (state.KeepRunning()) {
        deque.shrink_to_fit();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeShrinkToFit)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeClear(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }

    while (state.KeepRunning()) {
        deque.clear();
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeClear)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeInsert(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_back(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.insert(deque.begin() + rand()%N, large);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeInsert)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeErase(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        auto random = rand()%N;
        deque.insert(deque.begin() + random, large);
//        state.ResumeTiming();
        deque.erase(deque.begin() + random);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeErase)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequePushBack(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        deque.pop_front();
//        state.ResumeTiming();
        deque.push_back(large);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequePushBack)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequePopBack(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
        large.randomize();
        deque.push_back(large);
//        state.PauseTiming();
        deque.pop_front();
//        state.ResumeTiming();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequePopBack)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequePushFront(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        deque.pop_back();
//        state.ResumeTiming();
        deque.push_front(large);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequePushFront)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();


void largeDequePopFront(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        deque.push_front(large);
//        state.ResumeTiming();
        deque.pop_front();
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequePopFront)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeResize(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque.push_front(large);
    }
    while (state.KeepRunning()) {
        deque.resize(rand()%N);
        DoNotOptimize(deque);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeResize)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();

void largeDequeSwap(State& state) {
    auto N = state.range(0);
    std::deque<Large> deque1, deque2;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        deque1.push_back(large);
        large.randomize();
        deque2.push_back(large);
    }
    while (state.KeepRunning()) {
        deque1.swap(deque2);
        DoNotOptimize(deque1);
        DoNotOptimize(deque2);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeDequeSwap)->RangeMultiplier(2)->Range(1, 1<<9)->Complexity();