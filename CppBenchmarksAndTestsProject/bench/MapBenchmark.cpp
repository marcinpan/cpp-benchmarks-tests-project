#include "benchmark/benchmark.h"
using namespace benchmark;
#include "Small.h"
#include "Medium.h"
#include "Large.h"
#include <map>
//
//void randMapBenchmark(State& state) {
//    auto N = state.range(0);
//    std::vector<int> vector(N);
//    for(auto i=0u; i <= N; i++) {
//        vector[i] = rand() % N;
//    }
//    while (state.KeepRunning()) {
//
//        DoNotOptimize(vector[rand()%N]);
//    }
//}
//BENCHMARK(randMapBenchmark)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//
//void smallMapAt(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    std::vector<int> vector(N);
//    for(auto i=0u; i <= N; i++) {
//        vector[i] = rand() % N;
//    }
//
//    while (state.KeepRunning()) {
//        auto randNum = vector[rand()%N];
//        DoNotOptimize(map.at(randNum));
//
//    }
//
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapAt)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapOperator(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    std::vector<int> vector(N);
//    for(auto i=0u; i <= N; i++) {
//        vector[i] = rand() % N;
//    }
//
//    while (state.KeepRunning()) {
//        auto randNum = vector[rand()%N];
//        DoNotOptimize(map[randNum]);
//
//    }
//
//    state.SetComplexityN(N);
//}
//BENCHMARK(smallMapOperator)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapEmpty(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.empty());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapEmpty)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapSize(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.size());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapSize)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapMaxSize(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.max_size());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapMaxSize)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapClear(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//
//    while (state.KeepRunning()) {
//        map.clear();
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapClear)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapInsert(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    int random = 0;
//    while (state.KeepRunning()) {
////        state.PauseTiming();
//        random = rand()%N;
//        small.randomize();
//        map.erase(map.begin());
////        state.ResumeTiming();
//        map.insert(std::make_pair(random, small));
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapInsert)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapErase(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    int random = 0;
//    while (state.KeepRunning()) {
////        state.PauseTiming();
//        random = rand()%N;
//        small.randomize();
//        map.insert(std::make_pair(random, small));
////        state.ResumeTiming();
//        map.erase(random);
//        DoNotOptimize(map);
//        ClobberMemory();
//
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapErase)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapSwap(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map1, map2;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map1[i] = small;
//        small.randomize();
//        map2[i] = small;
//    }
//
//    while (state.KeepRunning()) {
//        map1.swap(map2);
//        DoNotOptimize(map1);
//        DoNotOptimize(map2);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapSwap)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapCount(State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        map.count(random);
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapCount)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapFind (State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.find(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapFind)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapEqualRange (State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.equal_range(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapEqualRange)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapLowerBound (State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.lower_bound(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapLowerBound)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
//void smallMapUpperBound (State& state) {
//    auto N = state.range(0);
//    std::map<int, Small> map;
//    Small small;
//    for (int i = 0u; i <= N; ++i) {
//        small.randomize();
//        map[i] = small;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.upper_bound(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(smallMapUpperBound)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();
//
///*-----MEDIUM----*/
//
//
//void mediumMapAt(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    std::vector<int> vector(N);
//    for(auto i=0u; i <= N; i++) {
//        vector[i] = rand() % N;
//    }
//
//    while (state.KeepRunning()) {
//        auto randNum = vector[rand()%N];
//        DoNotOptimize(map.at(randNum));
//
//    }
//
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapAt)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapOperator(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    std::vector<int> vector(N);
//    for(auto i=0u; i <= N; i++) {
//        vector[i] = rand() % N;
//    }
//
//    while (state.KeepRunning()) {
//        auto randNum = vector[rand()%N];
//        DoNotOptimize(map[randNum]);
//
//    }
//
//    state.SetComplexityN(N);
//}
//BENCHMARK(mediumMapOperator)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapEmpty(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.empty());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapEmpty)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapSize(State& state) {
//    auto N = state.range(0);
//
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.size());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapSize)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapMaxSize(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    while (state.KeepRunning()) {
//        DoNotOptimize(map.max_size());
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapMaxSize)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapClear(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//
//    while (state.KeepRunning()) {
//        map.clear();
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapClear)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapInsert(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    int random = 0;
//    while (state.KeepRunning()) {
////        state.PauseTiming();
//        random = rand()%N;
//        medium.randomize();
//        map.erase(map.begin());
////        state.ResumeTiming();
//        map.insert(std::make_pair(random, medium));
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapInsert)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapErase(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    int random = 0;
//    while (state.KeepRunning()) {
////        state.PauseTiming();
//        random = rand()%N;
//        medium.randomize();
//        map.insert(std::make_pair(random, medium));
////        state.ResumeTiming();
//        map.erase(random);
//        DoNotOptimize(map);
//        ClobberMemory();
//
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapErase)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapSwap(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map1, map2;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map1[i] = medium;
//        medium.randomize();
//        map2[i] = medium;
//    }
//
//    while (state.KeepRunning()) {
//        map1.swap(map2);
//        DoNotOptimize(map1);
//        DoNotOptimize(map2);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapSwap)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapCount(State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        map.count(random);
//        DoNotOptimize(map);
//        ClobberMemory();
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapCount)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapFind (State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.find(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapFind)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapEqualRange (State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.equal_range(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapEqualRange)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapLowerBound (State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.lower_bound(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapLowerBound)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
//
//void mediumMapUpperBound (State& state) {
//    auto N = state.range(0);
//    std::map<int, Medium> map;
//    Medium medium;
//    for (int i = 0u; i <= N; ++i) {
//        medium.randomize();
//        map[i] = medium;
//    }
//    auto random = 0;
//    while (state.KeepRunning()) {
//        random = rand()%N;
//        DoNotOptimize(map.upper_bound(random));
//    }
//    state.SetComplexityN(N);
//}
//
//BENCHMARK(mediumMapUpperBound)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();
///*LARGE*/

void largeMapAt(State& state) {
    auto N = state.range(0);

    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        DoNotOptimize(map.at(randNum));

    }

    state.SetComplexityN(N);
}

BENCHMARK(largeMapAt)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapOperator(State& state) {
    auto N = state.range(0);

    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    std::vector<int> vector(N);
    for(auto i=0u; i <= N; i++) {
        vector[i] = rand() % N;
    }

    while (state.KeepRunning()) {
        auto randNum = vector[rand()%N];
        DoNotOptimize(map[randNum]);

    }

    state.SetComplexityN(N);
}
BENCHMARK(largeMapOperator)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapEmpty(State& state) {
    auto N = state.range(0);

    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    while (state.KeepRunning()) {
        DoNotOptimize(map.empty());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapEmpty)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapSize(State& state) {
    auto N = state.range(0);

    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    while (state.KeepRunning()) {
        DoNotOptimize(map.size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapSize)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapMaxSize(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    while (state.KeepRunning()) {
        DoNotOptimize(map.max_size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapMaxSize)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapClear(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }

    while (state.KeepRunning()) {
        map.clear();
        DoNotOptimize(map);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapClear)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapInsert(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    int random = 0;
    while (state.KeepRunning()) {
//        state.PauseTiming();
        random = rand()%N;
        large.randomize();
        map.erase(map.begin());
//        state.ResumeTiming();
        map.insert(std::make_pair(random, large));
        DoNotOptimize(map);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapInsert)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapErase(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    int random = 0;
    while (state.KeepRunning()) {
//        state.PauseTiming();
        random = rand()%N;
        large.randomize();
        map.insert(std::make_pair(random, large));
//        state.ResumeTiming();
        map.erase(random);
        DoNotOptimize(map);
        ClobberMemory();

    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapErase)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapSwap(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map1, map2;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map1[i] = large;
        large.randomize();
        map2[i] = large;
    }

    while (state.KeepRunning()) {
        map1.swap(map2);
        DoNotOptimize(map1);
        DoNotOptimize(map2);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapSwap)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapCount(State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    auto random = 0;
    while (state.KeepRunning()) {
        random = rand()%N;
        map.count(random);
        DoNotOptimize(map);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapCount)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapFind (State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    auto random = 0;
    while (state.KeepRunning()) {
        random = rand()%N;
        DoNotOptimize(map.find(random));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapFind)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapEqualRange (State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    auto random = 0;
    while (state.KeepRunning()) {
        random = rand()%N;
        DoNotOptimize(map.equal_range(random));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapEqualRange)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapLowerBound (State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    auto random = 0;
    while (state.KeepRunning()) {
        random = rand()%N;
        DoNotOptimize(map.lower_bound(random));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapLowerBound)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeMapUpperBound (State& state) {
    auto N = state.range(0);
    std::map<int, Large> map;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        map[i] = large;
    }
    auto random = 0;
    while (state.KeepRunning()) {
        random = rand()%N;
        DoNotOptimize(map.upper_bound(random));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeMapUpperBound)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();