#include "benchmark/benchmark.h"
using namespace benchmark;
#include "Small.h"
#include "Medium.h"
#include "Large.h"
#include <unordered_set>

void smallUnorderedMultisetEmpty(State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.empty());
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetEmpty)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetSize)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetMaxSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.max_size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetMaxSize)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetClear (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        unordered_multiset.clear();
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetClear)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetInsert (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        unordered_multiset.erase(small);
//        state.ResumeTiming();
        unordered_multiset.insert(small);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetInsert)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();//Not More!

void smallUnorderedMultisetErase (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        small.randomize();
        unordered_multiset.insert(small);
//        state.ResumeTiming();
        unordered_multiset.erase(small);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetErase)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();//Not More!

void smallUnorderedMultisetSwap (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset, unordered_multiset1;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
        small.randomize();
        unordered_multiset1.insert(small);
    }
    while (state.KeepRunning()) {
        swap(unordered_multiset, unordered_multiset1);
        DoNotOptimize(unordered_multiset);
        DoNotOptimize(unordered_multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetSwap)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetCount (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        small.randomize();
        DoNotOptimize(unordered_multiset.count(small));
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetCount)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetFind (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        small.randomize();
        DoNotOptimize(unordered_multiset.find(small));
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetFind)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetEqualRange (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    for (int i = 0u; i <= N; ++i) {
        small.randomize();
        unordered_multiset.insert(small);
    }
    while (state.KeepRunning()) {
        small.randomize();
        DoNotOptimize(unordered_multiset.equal_range(small));
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetEqualRange)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetRehash (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Small> unordered_multiset;
        unordered_multiset.rehash(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetRehash)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

void smallUnorderedMultisetReserve (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Small> unordered_multiset;
        unordered_multiset.reserve(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(smallUnorderedMultisetReserve)->RangeMultiplier(2)->Range(1, 1<<20)->Complexity();

/*-----MEDIUM-----*/

void mediumUnorderedMultisetEmpty(State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.empty());
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetEmpty)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetSize)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetMaxSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.max_size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetMaxSize)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetClear (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        unordered_multiset.clear();
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetClear)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetInsert (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        unordered_multiset.erase(medium);
//        state.ResumeTiming();
        unordered_multiset.insert(medium);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetInsert)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();//Not More!

void mediumUnorderedMultisetErase (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        medium.randomize();
        unordered_multiset.insert(medium);
//        state.ResumeTiming();
        unordered_multiset.erase(medium);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetErase)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();//Not More!

void mediumUnorderedMultisetSwap (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset, unordered_multiset1;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
        medium.randomize();
        unordered_multiset1.insert(medium);
    }
    while (state.KeepRunning()) {
        swap(unordered_multiset, unordered_multiset1);
        DoNotOptimize(unordered_multiset);
        DoNotOptimize(unordered_multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetSwap)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetCount (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        medium.randomize();
        DoNotOptimize(unordered_multiset.count(medium));
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetCount)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetFind (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        medium.randomize();
        DoNotOptimize(unordered_multiset.find(medium));
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetFind)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetEqualRange (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    for (int i = 0u; i <= N; ++i) {
        medium.randomize();
        unordered_multiset.insert(medium);
    }
    while (state.KeepRunning()) {
        medium.randomize();
        DoNotOptimize(unordered_multiset.equal_range(medium));
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetEqualRange)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetRehash (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Medium> unordered_multiset;
        unordered_multiset.rehash(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetRehash)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

void mediumUnorderedMultisetReserve (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Medium> unordered_multiset;
        unordered_multiset.reserve(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(mediumUnorderedMultisetReserve)->RangeMultiplier(2)->Range(1, 1<<14)->Complexity();

/*----LARGE---*/

void largeUnorderedMultisetEmpty(State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.empty());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetEmpty)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetSize)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetMaxSize (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    while (state.KeepRunning()) {
        DoNotOptimize(unordered_multiset.max_size());
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetMaxSize)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetClear (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        unordered_multiset.clear();
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetClear)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetInsert (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        unordered_multiset.erase(large);
//        state.ResumeTiming();
        unordered_multiset.insert(large);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetInsert)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();//Not More!

void largeUnorderedMultisetErase (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
//        state.PauseTiming();
        large.randomize();
        unordered_multiset.insert(large);
//        state.ResumeTiming();
        unordered_multiset.erase(large);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetErase)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();//Not More!

void largeUnorderedMultisetSwap (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset, unordered_multiset1;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
        large.randomize();
        unordered_multiset1.insert(large);
    }
    while (state.KeepRunning()) {
        swap(unordered_multiset, unordered_multiset1);
        DoNotOptimize(unordered_multiset);
        DoNotOptimize(unordered_multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetSwap)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetCount (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        large.randomize();
        DoNotOptimize(unordered_multiset.count(large));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetCount)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetFind (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        large.randomize();
        DoNotOptimize(unordered_multiset.find(large));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetFind)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetEqualRange (State& state) {
    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    for (int i = 0u; i <= N; ++i) {
        large.randomize();
        unordered_multiset.insert(large);
    }
    while (state.KeepRunning()) {
        large.randomize();
        DoNotOptimize(unordered_multiset.equal_range(large));
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetEqualRange)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetRehash (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Large> unordered_multiset;
        unordered_multiset.rehash(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetRehash)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();

void largeUnorderedMultisetReserve (State& state) {
    auto N = state.range(0);

    while (state.KeepRunning()) {
        std::unordered_multiset<Large> unordered_multiset;
        unordered_multiset.reserve(N);
        DoNotOptimize(unordered_multiset);
        ClobberMemory();
    }
    state.SetComplexityN(N);
}

BENCHMARK(largeUnorderedMultisetReserve)->RangeMultiplier(2)->Range(1, 1<<8)->Complexity();
