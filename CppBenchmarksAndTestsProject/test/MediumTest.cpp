#include "gtest/gtest.h"

#include <vector>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Medium.h"
#include <iostream>
using namespace std;


TEST(MediumTest, size) {
    EXPECT_EQ(1024u, sizeof(Medium))  << "Size is not 1KB!";
}

// Dummy tests to check that everything compiles

TEST(MediumTest, createObject) {
    Medium medium;
    EXPECT_EQ(1024u, sizeof(medium));
}

TEST(MediumTest, hasLessThenOperator) {
    Medium a;
    Medium b;
    for (int i = 0; i < Medium::SIZE; ++i) {
        a.data[i] = i;
        b.data[i] = i+1;
    }
    EXPECT_LT(a, b);
}

TEST(MediumTest, hasEqualityOperator) {
    Medium a = {0};
    Medium b = {0};
    EXPECT_EQ(a,b);
}

TEST(MediumTest, canBeHashed) {
    Medium medium;
    medium.randomize();
    std::hash<Medium> hash;
    EXPECT_TRUE(hash(medium));
}

TEST(MediumTest, collections) {

    Medium medium;
    medium.randomize();
    std::vector<Medium> vector;
    vector.push_back(medium);
    EXPECT_EQ(medium, vector.back());

    std::array<Medium, 1> array;
    array[0] = medium;
    EXPECT_EQ(medium, array.at(0));

    std::deque<Medium> deque;
    deque.push_back(medium);
    EXPECT_EQ(medium, deque.back());

    std::list<Medium> list;
    list.push_back(medium);
    EXPECT_EQ(medium, list.back());

    std::forward_list<Medium> forward_list;
    forward_list.push_front(medium);
    EXPECT_EQ(medium, forward_list.front());

    std::map<Medium, bool> map;
    map[medium] = true;
    EXPECT_TRUE(map[medium]);

    std::set<Medium> set;
    set.insert(medium);
    EXPECT_EQ(1u, set.count(medium));
    EXPECT_EQ(1u, set.size());

    std::unordered_map<Medium, bool> unordered_map;
    unordered_map[medium] = true;
    EXPECT_TRUE(unordered_map[medium]);
    EXPECT_EQ(1u, unordered_map.size());
    EXPECT_EQ(1u, unordered_map.count(medium));

    std::unordered_set<Medium> unordered_set;
    unordered_set.insert(medium);
    EXPECT_EQ(1u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(medium));
}


TEST(MediumTest, randomize) {

    Medium medium;
    medium.randomize();

    auto count = 0u;

    for (auto i=0u; i < Medium::SIZE; i++) {
        ASSERT_LE(0, medium.data[i]);
        ASSERT_GE(std::numeric_limits<int>::max(), medium.data[i]);

        if (medium.data[i] != 0)
            ++count;
    }

    EXPECT_NE(0u, count) << "All elements were zero?";
}

TEST(MediumTest, clear) {
    Medium medium;
    medium.randomize();
    medium.clear();
    for (auto i = 0u; i < Medium::SIZE; i++) {
        ASSERT_EQ(0, medium.data[i]);
    }
}

// TODO: Add tests for your operators implementation!

TEST(MediumTest, hashFunction) {
    std::unordered_set<size_t> unordered_set;
    std::hash<Medium> hash;

    for (auto i = 0u; i < 1024u; i++) {
        Medium medium = {0};
        for (auto j = 0u; j < Medium::SIZE; ++j) {
            medium.data[j] = j + i;
        }
        unordered_set.insert(hash(medium));

    }
    EXPECT_EQ(1024u, unordered_set.size());// <- output of hash(small) did not duplicate
}