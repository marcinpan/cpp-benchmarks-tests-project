#include "gtest/gtest.h"

#include <vector>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Small.h"

TEST(SmallTest, size) {
    EXPECT_EQ(1u, sizeof(Small))  << "Size is not 1B!";
}

// Dummy tests to check that everything compiles

TEST(SmallTest, createObject) {
    Small small;
    EXPECT_EQ(1u, sizeof(small));
}

TEST(SmallTest, hasLessThenOperator) {
    Small a = {'a'};
    Small b = {'b'};
    EXPECT_LT(a, b);

}

TEST(SmallTest, hasEqualityOperator) {
    Small a = {'a'};
    Small b = {'a'};
    EXPECT_EQ(a, b);
}

TEST(SmallTest, canBeHashed) {
    Small small;
    small.randomize();
    std::hash<Small> hash;
    EXPECT_TRUE(hash(small));
}

TEST(SmallTest, collections) {

    Small small;
    small.randomize();
    std::vector<Small> vector;
    vector.push_back(small);
    EXPECT_EQ(small, vector.back());

    std::array<Small, 1> array;
    array[0] = small;
    EXPECT_EQ(small, array.at(0));

    std::deque<Small> deque;
    deque.push_back(small);
    EXPECT_EQ(small, deque.back());

    std::list<Small> list;
    list.push_back(small);
    EXPECT_EQ(small, list.back());

    std::forward_list<Small> forward_list;
    forward_list.push_front(small);
    EXPECT_EQ(small, forward_list.front());

    std::map<Small, bool> map;
    map[small] = true;
    EXPECT_TRUE(map[small]);

    std::set<Small> set;
    set.insert(small);
    EXPECT_EQ(1u, set.count(small));
    EXPECT_EQ(1u, set.size());

    std::unordered_map<Small, bool> unordered_map;
    unordered_map[small] = true;
    EXPECT_TRUE(unordered_map[small]);
    EXPECT_EQ(1u, unordered_map.size());
    EXPECT_EQ(1u, unordered_map.count(small));

    std::unordered_set<Small> unordered_set;
    unordered_set.insert(small);
    unordered_set.insert(small);
    EXPECT_EQ(1u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(small));
}

TEST(SmallTest, randomize) {

    auto count = 0u;

    for (auto n = 0u; n < 1024u; n++) {

        Small small;
        small.randomize();

        for (auto i = 0u; i < Small::SIZE; i++) {

            ASSERT_LE(-128, small.data[i]);
            ASSERT_GE(127, small.data[i]);

            if (small.data[i] != 0)
                ++count;
        }
    }

    EXPECT_NE(0u, count) << "All elements were zero?";
}

TEST(SmallTest, clear) {
    Small small;
    small.randomize();
    small.clear();
    for (auto i = 0u; i < Small::SIZE; i++) {
        ASSERT_EQ(0, small.data[i]);
    }
}


// TODO: Add tests for your operators implementation!

TEST(SmallTest, hashFunction) {
    std::unordered_set<size_t> unordered_set;
    std::hash<Small> hash;

    for (auto i = 0u; i < 256u; i++) {  //max length of char - 1byte
                                        //if exceeded, converts to 256
        Small small;
        small = {(char)i};
        unordered_set.insert(hash(small));
    }
    EXPECT_EQ(256u, unordered_set.size());// <- output of hash(small) did not duplicate
}

