#include "gtest/gtest.h"

#include <vector>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Large.h"

TEST(LargeTest, size) {
    EXPECT_EQ(1024u*1024u, sizeof(Large)) << "Size is not 1MB!";
}

// Dummy tests to check that everything compiles

TEST(LargeTest, createObject) {
    Large large;
    EXPECT_EQ(1024u*1024u, sizeof(large));
}

TEST(LargeTest, hasLessThenOperator) {
    Large a;
    Large b;
    for (int i = 0; i < Large::SIZE; ++i) {
        a.data[i] = i;
        b.data[i] = i+1;
    }
    EXPECT_LT(a, b);
}

TEST(LargeTest, hasEqualityOperator) {
    Large a = {0};
    Large b = {0};
    EXPECT_EQ(a,b);
}

TEST(LargeTest, canBeHashed) {
    Large large;
    large.randomize();
    std::hash<Large> hash;
    EXPECT_TRUE(hash(large));
}

TEST(LargeTest, collections) {

    Large large;
    large.randomize();
    std::vector<Large> vector;
    vector.push_back(large);
    EXPECT_EQ(large, vector.back());

    std::array<Large, 1> array;
    array[0] = large;
    EXPECT_EQ(large, array.at(0));

    std::deque<Large> deque;
    deque.push_back(large);
    EXPECT_EQ(large, deque.back());

    std::list<Large> list;
    list.push_back(large);
    EXPECT_EQ(large, list.back());

    std::forward_list<Large> forward_list;
    forward_list.push_front(large);
    EXPECT_EQ(large, forward_list.front());

    std::map<Large, bool> map;
    map[large] = true;
    EXPECT_TRUE(map[large]);

    std::set<Large> set;
    set.insert(large);
    EXPECT_EQ(1u, set.count(large));
    EXPECT_EQ(1u, set.size());

    std::unordered_map<Large, bool> unordered_map;
    unordered_map[large] = true;
    EXPECT_TRUE(unordered_map[large]);
    EXPECT_EQ(1u, unordered_map.size());
    EXPECT_EQ(1u, unordered_map.count(large));


    std::unordered_set<Large> unordered_set;
    unordered_set.insert(large);
    EXPECT_EQ(1u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(large));
}

TEST(LargeTest, randomize) {

    Large large;
    large.randomize();

    auto count = 0u;

    for (auto i=0u; i < Large::SIZE; i++) {

        ASSERT_LE(0.0, large.data[i]);
        ASSERT_GE(1.0, large.data[i]);

        if (large.data[i] != 0.0)
            ++count;
    }

    EXPECT_NE(0u, count) << "All elements were zero?";
}

TEST(LargeTest, clear) {
    Large large;
    large.randomize();
    large.clear();
    for (auto i = 0u; i < Large::SIZE; i++) {
        ASSERT_DOUBLE_EQ(0.0, large.data[i]);
    }
}


// TODO: Add tests for your operators implementation!

TEST(LargeTest, hashFunction) {
    std::unordered_set<size_t> unordered_set;
    std::hash<Large> hash;

    for (auto i = 0u; i < 256u; i++) {
        Large large = {0};
        for (auto j = 0u; j < Large::SIZE; ++j) {
            large.data[j] = j + i;
        }

        unordered_set.insert(hash(large));

    }
    EXPECT_EQ(256u, unordered_set.size());// <- output of hash() did not duplicate
}