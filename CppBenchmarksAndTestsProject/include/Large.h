#pragma once

struct Large {

    constexpr static unsigned SIZE = 128u*1024u;

    double data[SIZE];

    void clear() {
        memset(data, 0, SIZE*sizeof(double));
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_real_distribution<> dis{0.0, 1.0};

        for (auto i=0u; i < SIZE; i++) {
            data[i] = dis(gen);
        }
    }

    bool operator<(const Large &rhs) const {
        for(auto i=0u; i < SIZE; i++) {
            if (data[i] < rhs.data[i]) continue;
            else return false;
        }
        return true;
    }

    bool operator==(const Large &rhs) const {
        for (auto i = 0u; i < SIZE; ++i) {
            if (data[i] == rhs.data[i]);
            else return false;
        }
        return true;
    }
};
//for (auto i = 0u; i < d.SIZE; ++i) {
//hash_sum += hash( (int)(d.data[i]*10000000000) ) << i;
//}
//return hash_sum;
namespace std {
    template<>
    struct hash<Large> {
        std::size_t operator()(const Large &d) const {
            std::hash<double> hash;
            auto hash_sum = 0;
            hash_sum += ( ((int)(d.data[0]*10000000))     << 1) +
                        ( ((int)(d.data[d.SIZE/2]*10000)) << 2) +
                        ( ((int)(d.data[d.SIZE]*1000))    << 3);
            return hash_sum;
        }
    };
}
