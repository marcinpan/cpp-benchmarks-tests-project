#pragma once
#include <string.h>
#include <random>

using namespace std;

struct Small {

    constexpr static unsigned SIZE = 1u;
    char data[SIZE];

    void clear() {
        memset(data, 0, SIZE*sizeof(char));
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_int_distribution<> dis{-128, 127};

        for (auto i=0u; i < SIZE; i++) {
            data[i] = (char) dis(gen);
        }
    }

    bool operator<(const Small &rhs) const {
        for(auto i=0u; i < SIZE; i++) {
            if (data[i] < rhs.data[i]);
            else return false;
        }
        return true;
    }

    bool operator==(const Small &rhs) const {
        for (auto i = 0u; i < SIZE; ++i) {
            if (data[i] == rhs.data[i]);
            else return false;
        }
        return true;
    }
};

namespace std {
    template<>
    struct hash<Small> {
        std::size_t operator()(const Small &d) const {
            std::hash<int> hash;
            auto hash_sum = 0;
            for (auto i = 0u; i < d.SIZE; ++i) {
                 hash_sum += hash(d.data[i]) << i;
            }
            return hash_sum;
        }
    };
}
