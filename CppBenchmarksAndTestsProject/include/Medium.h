#pragma once
#include <string.h>
#include <random>

struct Medium {

    constexpr static unsigned SIZE = 256u;
    int data[SIZE];

    void clear() {
        memset(data, 0, SIZE*sizeof(int));
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_int_distribution<> dis{0,std::numeric_limits<int>::max()};

        for (auto i=0u; i < SIZE; i++) {
            data[i] = dis(gen);
        }
    }

    bool operator<(const Medium &rhs) const {
        for(auto i=0u; i < SIZE; i++) {
            if (data[i] < rhs.data[i]) continue;
            else return false;
        }
        return true;
    }

    bool operator==(const Medium &rhs) const {
        for (auto i = 0u; i < SIZE; ++i) {
            if (data[i] == rhs.data[i]);
            else return false;
        }
        return true;
    }
};

namespace std {
    template<>
    struct hash<Medium> {
        std::size_t operator()(const Medium &d) const {
            std::hash<int> hash;
            auto hash_sum = 0;
            for (auto i = 0u; i < d.SIZE; ++i) {
                hash_sum += hash(d.data[i]) << i;
            }
            return hash_sum;
        }
    };
}