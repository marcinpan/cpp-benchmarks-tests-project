# Instructions

This is a starter project that should be used to realize the STL performance analysis assignment.

What needs to be done?

0) Copy this project to your repository

1) Implement missing operators and hash function for **Small**, **Medium** and **Large** structures

2) Add unit tests to make sure that operators and hash function work correctly (you can optionally create benchmarks for those methods)

3) Create micro-benchmarks for all methods in selected containers interfaces

   One example might be to create benchmarks for **std::vector**, **std::set** and **std::unordered_map**.
   You should first select the **Small** structure go through all containers one by one. Let's say that you want to start 
   with **std::vector**: in that case first check all methods that are provided in that container interface and then 
   create benchmarks for every one of them. Some benchmarks will be very easy (like the **size()** method) and some 
   other will be a little bit more difficult (example might be the **push_back()** method).
   When you are done with first container repeat the same for remaining containers.
   Last step will be to duplicate your benchmarks for two remaining structure sizes.
   
   How many benchmarks should I create?
   
   B - number of benchmarks to create
   
   Ni - number of methods in i-th container interface (we have three: N1, N2 and N3)
   
   S - number for structure size variants (in our case it is 3)

   **B = ( N1 + N2 + N3 ) * S**

   Vast majority of the benchmark code should be simple copy-paste :)
   
4) Run benchmarks in **Debug** mode without any optimizations.

   Simply compile your application in Debug mode (that corresponds to **-O0** flag in **gcc**).
   This step will be important because we want to get some baselines for test with optimization enabled.
   Writing benchmarks in that mode is quite easy and we can get some number and even more importantly we can have our
   first guesses on the algorithmic complexity (the big O notation e.g.: O(1), O(N), O(log(N)) etc.).
   
5) Use appropriate escape functions to make sure that your code is not deleted by optimizer and rerun benchmarks in **Release** mode.

   Release mode corresponds to **-O2** or **-O3** flags enabled in **gcc**.
   
6) Create **PDF** document with your performance analysis results

   What should it contain?
   
   - Hardware information (**CPU**, **RAM** etc.)
   
   - Brief description of containers implementation (only for those that you have tested)
   
      a. what kind of data structure is used under the hood?
      
      b. what is the expected performance (which operations should be fast or slow)
   
   - Brief description on how you implemented operators "<", "==" and hash function from point 2)
   
     a. Why do we even need to add them?
     
     b. What happens when tey are poorly implemented?
   
   - Raw data and graphs from your benchmarks
   
     Each graph should have two axis: the X axis will have input size of given collection and the Y axis will 
     present the time from our benchmarks (we will get that data for free after running benchmarks - data can be exported to CSV format).
     Every graph should have six lines plotted: for every structure size (we have three) and for Debug/Release mode (3*2 = 6).
     There should be one graph for every method (N1+N2+N3 graphs in total).
     You should also attach raw data (e.g.: in some kind of simple table next to given diagram).
     
   - Results summary and conclusions
   
     Which operations are fast and which are slow? What operations should be avoided?
     How does the structure size influence performance? How does optimization help?
     Any other thoughts on how performance in impacted by other factors.
     
7) Commit all your changes and the PDF file to your repository

   Projects will be graded based on what was committed to your repository (under lesson_05).
   The PDF file should be also added in that directory: **lesson_05/report.pdf**
   
You have time to finish project and commit all changes until **13.11.2016 (23:59)**.
There are **4** points to get (operator implementation, unit tests, benchmarks and report).

Tips:

- Elements inserted into collections should not be empty: use **randomize()** method before inserting
- You can also use **clear()** method to make sure that object has all values set to zero

### Which methods should be tested?

#### std::vector
    
        at, operator[], front, back, data,
        empty, size, max_size, reserve, capacity, shrink_to_fit, 
        clear, insert, erase, push_back, pop_back, resize, swap

#### std::deque
    
        at, operator[], front, back,
        empty, size, max_size, shrink_to_fit,
        clear, insert, erase, push_back, pop_back, push_front, pop_front, resize, swap

#### std::list

        front, back, empty, size, max_size,
        clear, insert, erase, push_back, pop_back, push_front, pop_front, resize, swap,
        merge, splice, remove, remove_if, reverse, unique, sort
        
#### std::forward_list

        front, empty, max_size,
        clear, insert_after, erase_after, push_front, pop_front, resize, swap
        merge, splice_after, remove, remove_if, reverse, unique, sort

#### std::set
    
        empty, size, max_size, 
        clear, insert, erase, swap,
        count, find, equal_range, lower_bound, upper_bound

#### std::map
    
        at, operator[], empty, size, max_size, 
        clear, insert, erase, swap,
        count, find, equal_range, lower_bound, upper_bound

#### std::multiset

        empty, size, max_size, 
        clear, insert, erase, swap,
        count, find, equal_range, lower_bound, upper_bound
        
#### std::multimap

        empty, size, max_size,
        clear, insert, erase, swap, 
        count, find, equal_range, lower_bound, upper_bound

#### std::unordered_set
    
        empty, size, max_size, 
        clear, insert, erase, swap,
        count, find, equal_range,
        rehash, reserve

#### std::unordered_map
    
        empty, size, max_size, 
        clear, insert, erase, swap,
        at, operator[], count, find, equal_range,
        rehash, reserve

#### std::unordered_multiset

        empty, size, max_size, 
        clear, insert, erase, swap,
        count, find, equal_range,
        rehash, reserve
        
#### std::unordered_multimap

        empty, size, max_size,
        clear, insert, erase, swap, 
        count, find, equal_range,
        rehash, reserve

You can always test more methods if you find it important to prove some point.
    
    
    
    
    
    
    
    
    
    
    
    
    